<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:12 AM
 */

namespace Smorken\Logger;

use Smorken\Service\Service;

class LoggerService extends Service {

    protected $deferred = false;

    protected $provides = array();

    public function start()
    {
        $this->name = 'loggers';
    }

    /**
     * Creates loggers from Monolog\Logger for each option in config/log
     * Binds each logger into a provider called 'logger.logger_name'
     */
    public function load()
    {
        $config = $this->app['config']['log'];
        if ($config) {
            $loggers = LoggerFactory::create('Monolog\Logger', $config);
            foreach($loggers as $name => $logger) {
                $this->provides = array('logger.' . $name);
                $this->app['logger.' . $name] = function($c) use ($logger) {
                    return $logger;
                };
            }
        }
    }

    public function provides()
    {
        return $this->provides;
    }
} 