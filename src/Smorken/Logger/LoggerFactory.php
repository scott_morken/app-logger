<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 9:09 AM
 */

namespace Smorken\Logger;


class LoggerFactory {

    /**
     * Creates loggers from the config using $loggercls
     * @param string $loggercls
     * @param array $config
     * @return array
     */
    public static function create($loggercls, $config)
    {
        $loggers = array();
        foreach($config['loggers'] as $name => $loggerinfo) {
            $loggers[$name] = new $loggercls($name);
            $handlers = self::createHandlers($loggerinfo);
            foreach($handlers as $handler) {
                $loggers[$name]->pushHandler($handler);
            }
            $processors = self::createProcessors($loggerinfo);
            foreach($processors as $processor) {
                $loggers[$name]->pushProcessor($processor);
            }
        }
        return $loggers;
    }

    protected static function createFormatters($info)
    {
        return self::createItems($info, 'formatters');
    }

    protected static function createHandlers($info)
    {
        $formatters = self::createFormatters($info);
        $handlers = self::createItems($info, 'handlers');
        foreach($formatters as $formatter) {
            foreach($handlers as $handler) {
                $handler->setFormatter($formatter);
            }
        }
        return $handlers;
    }

    protected static function createItems($info, $key) {
        $items = array();
        if (isset($info[$key])) {
            foreach($info[$key] as $cls => $args) {
                $reflector = new \ReflectionClass($cls);
                $items[] = $reflector->newInstanceArgs($args);
            }
        }
        return $items;
    }

    protected static function createProcessors($info)
    {
        $proc = array();
        if (isset($info['processors'])) {
            foreach($info['processors'] as $processor) {
                $proc[] = $processor;
            }
        }
        return $proc;
    }
} 