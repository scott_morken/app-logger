## Standalone
~~~~
<?php
include 'vendors/autoload.php';
$config = array();
$loggers = LoggerFactory::create('Monolog\Logger', $config);
foreach($loggers as $name => $logger) {
    //add to some DI container for later use
    $this->app['logger.' . $name] = function($c) use ($logger) {
        return $logger;
    };
}
~~~~

## Part of simple app

Should automatically be included if using composer to set up the project

Otherwise add a service line to config/app.php services array

```
'Smorken\Logger\LoggerService'
```